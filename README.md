# Microtelephone

This is a replica of the service microtelephone adapter for the original Iskra's FDM: Abfr 302.

The original device looked like this:

![Original device](Images/Original.jpg)
