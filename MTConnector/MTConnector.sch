EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5FF0BB94
P 4050 3100
F 0 "J3" H 3968 3617 50  0000 C CNN
F 1 "Conn_01x08" H 3968 3526 50  0000 C CNN
F 2 "MTConn:MTConn" H 4050 3100 50  0001 C CNN
F 3 "~" H 4050 3100 50  0001 C CNN
	1    4050 3100
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5FF0D638
P 5750 2800
F 0 "J2" H 5830 2842 50  0000 L CNN
F 1 "Conn_01x01" H 5830 2751 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5750 2800 50  0001 C CNN
F 3 "~" H 5750 2800 50  0001 C CNN
	1    5750 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR01
U 1 1 5FF0DBA4
P 4450 2500
F 0 "#PWR01" H 4450 2350 50  0001 C CNN
F 1 "+24V" H 4465 2673 50  0000 C CNN
F 2 "" H 4450 2500 50  0001 C CNN
F 3 "" H 4450 2500 50  0001 C CNN
	1    4450 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5FF0E158
P 4450 3650
F 0 "#PWR02" H 4450 3400 50  0001 C CNN
F 1 "GND" H 4455 3477 50  0000 C CNN
F 2 "" H 4450 3650 50  0001 C CNN
F 3 "" H 4450 3650 50  0001 C CNN
	1    4450 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 2900 4450 2900
Wire Wire Line
	4450 2900 4450 2500
Wire Wire Line
	4250 3200 4450 3200
Wire Wire Line
	4450 3200 4450 3650
Text Label 5250 2600 0    50   ~ 0
micA
Text Label 5250 2800 0    50   ~ 0
micB
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5FF10C4E
P 5750 3100
F 0 "J4" H 5830 3142 50  0000 L CNN
F 1 "Conn_01x01" H 5830 3051 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5750 3100 50  0001 C CNN
F 3 "~" H 5750 3100 50  0001 C CNN
	1    5750 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 5FF10EA6
P 5750 3300
F 0 "J5" H 5830 3342 50  0000 L CNN
F 1 "Conn_01x01" H 5830 3251 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5750 3300 50  0001 C CNN
F 3 "~" H 5750 3300 50  0001 C CNN
	1    5750 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2800 4900 2800
Wire Wire Line
	4900 2800 4900 3000
Wire Wire Line
	4900 3000 4250 3000
Wire Wire Line
	4250 2800 4800 2800
Wire Wire Line
	4800 2800 4800 2600
Wire Wire Line
	4800 2600 5550 2600
Wire Wire Line
	4250 3100 5550 3100
Wire Wire Line
	4250 3300 5550 3300
Text Label 5250 3100 0    50   ~ 0
rx_ring
Text Label 5250 3300 0    50   ~ 0
tx_ring
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 5FF163EE
P 5750 3600
F 0 "J6" H 5830 3642 50  0000 L CNN
F 1 "Conn_01x01" H 5830 3551 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5750 3600 50  0001 C CNN
F 3 "~" H 5750 3600 50  0001 C CNN
	1    5750 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 5FF173F1
P 5750 3800
F 0 "J7" H 5830 3842 50  0000 L CNN
F 1 "Conn_01x01" H 5830 3751 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5750 3800 50  0001 C CNN
F 3 "~" H 5750 3800 50  0001 C CNN
	1    5750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3400 4900 3400
Wire Wire Line
	4900 3400 4900 3600
Wire Wire Line
	4900 3600 5550 3600
Wire Wire Line
	4250 3500 4800 3500
Wire Wire Line
	4800 3500 4800 3800
Wire Wire Line
	4800 3800 5550 3800
Text Label 5250 3600 0    50   ~ 0
spkA
Text Label 5250 3800 0    50   ~ 0
spkB
$Comp
L Mechanical:MountingHole H4
U 1 1 5FF1A57D
P 6300 7650
F 0 "H4" H 6400 7696 50  0000 L CNN
F 1 "MountingHole" H 6400 7605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 6300 7650 50  0001 C CNN
F 3 "~" H 6300 7650 50  0001 C CNN
	1    6300 7650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FF1AFEE
P 6300 7450
F 0 "H3" H 6400 7496 50  0000 L CNN
F 1 "MountingHole" H 6400 7405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 6300 7450 50  0001 C CNN
F 3 "~" H 6300 7450 50  0001 C CNN
	1    6300 7450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FF1B0EE
P 6300 7250
F 0 "H2" H 6400 7296 50  0000 L CNN
F 1 "MountingHole" H 6400 7205 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 6300 7250 50  0001 C CNN
F 3 "~" H 6300 7250 50  0001 C CNN
	1    6300 7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5FF1B1D7
P 6300 7050
F 0 "H1" H 6400 7096 50  0000 L CNN
F 1 "MountingHole" H 6400 7005 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO14580_Pad" H 6300 7050 50  0001 C CNN
F 3 "~" H 6300 7050 50  0001 C CNN
	1    6300 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 5FF0D1AC
P 5750 2600
F 0 "J1" H 5830 2642 50  0000 L CNN
F 1 "Conn_01x01" H 5830 2551 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5750 2600 50  0001 C CNN
F 3 "~" H 5750 2600 50  0001 C CNN
	1    5750 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR03
U 1 1 5FF2246A
P 7400 2500
F 0 "#PWR03" H 7400 2350 50  0001 C CNN
F 1 "+24V" H 7415 2673 50  0000 C CNN
F 2 "" H 7400 2500 50  0001 C CNN
F 3 "" H 7400 2500 50  0001 C CNN
	1    7400 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FF23669
P 7400 2900
F 0 "#PWR04" H 7400 2650 50  0001 C CNN
F 1 "GND" H 7405 2727 50  0000 C CNN
F 2 "" H 7400 2900 50  0001 C CNN
F 3 "" H 7400 2900 50  0001 C CNN
	1    7400 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 5FF24C74
P 7700 2600
F 0 "J8" H 7780 2642 50  0000 L CNN
F 1 "Conn_01x01" H 7780 2551 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 7700 2600 50  0001 C CNN
F 3 "~" H 7700 2600 50  0001 C CNN
	1    7700 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 5FF2520A
P 7700 2800
F 0 "J9" H 7780 2842 50  0000 L CNN
F 1 "Conn_01x01" H 7780 2751 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 7700 2800 50  0001 C CNN
F 3 "~" H 7700 2800 50  0001 C CNN
	1    7700 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2800 7400 2800
Wire Wire Line
	7400 2800 7400 2900
Wire Wire Line
	7500 2600 7400 2600
Wire Wire Line
	7400 2600 7400 2500
$EndSCHEMATC
